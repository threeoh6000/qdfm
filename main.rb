require 'rss'
require 'nokogiri'
require 'open-uri'

doc = Nokogiri::HTML(URI.open("https://colean.cc/journal/2021/index.html"))
titles = []
locs = []

doc.css('a').each do |link|
	if link.content.include? "[.., d]"
	else
		titles << link.content
		locs << "https://colean.cc/journal/2021/" + link['href']
	end
end

titles = titles.reverse
locs = locs.reverse

feed = RSS::Maker.make("rss2.0") do |maker|
	maker.channel.author = "QDFM"
	maker.channel.updated = Time.now.to_s
	maker.channel.about = "https://colean.cc/journal/rss.xml"
	maker.channel.title = "The Journal" 
	maker.channel.description = "The trihosted thoughts of Celeste"
	maker.channel.link = "https://colean.cc/journal"

	i = -1
	loop do
		i = i + 1
		maker.items.new_item do |item|
			item.link = locs[i]
			item.title = titles[i]
			item.updated = Time.now.to_s
		end
		if i == titles.length - 1
			break
		end
	end
end

puts feed
